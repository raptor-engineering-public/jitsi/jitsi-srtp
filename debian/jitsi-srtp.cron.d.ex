#
# Regular cron jobs for the jitsi-srtp package.
#
0 4	* * *	root	[ -x /usr/bin/jitsi-srtp_maintenance ] && /usr/bin/jitsi-srtp_maintenance
