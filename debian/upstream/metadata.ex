# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/jitsi-srtp/issues
# Bug-Submit: https://github.com/<user>/jitsi-srtp/issues/new
# Changelog: https://github.com/<user>/jitsi-srtp/blob/master/CHANGES
# Documentation: https://github.com/<user>/jitsi-srtp/wiki
# Repository-Browse: https://github.com/<user>/jitsi-srtp
# Repository: https://github.com/<user>/jitsi-srtp.git
