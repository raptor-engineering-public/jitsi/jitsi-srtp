Document: jitsi-srtp
Title: Debian jitsi-srtp Manual
Author: <insert document author here>
Abstract: This manual describes what jitsi-srtp is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/jitsi-srtp/jitsi-srtp.sgml.gz

Format: postscript
Files: /usr/share/doc/jitsi-srtp/jitsi-srtp.ps.gz

Format: text
Files: /usr/share/doc/jitsi-srtp/jitsi-srtp.text.gz

Format: HTML
Index: /usr/share/doc/jitsi-srtp/html/index.html
Files: /usr/share/doc/jitsi-srtp/html/*.html
